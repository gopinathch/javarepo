/*******************************************************************************
 * Copyright (c) 2009, 2015 Mountainminds GmbH & Co. KG and Contributors
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc R. Hoffmann - initial API and implementation
 *    
 *******************************************************************************/
package org.jacoco.examples.parser;

import java.io.IOException;
import junit.framework.Assert;
import junit.framework.TestCase;

public class ExpressionParserTest extends TestCase {

//	@Test
	public void testexpression1() throws IOException {
		assertExpression("2 * 3 + 4", 10);
	}

//	@Test
	public void testexpression2() throws IOException {
		assertExpression("2 + 3 * 4", 14);
	}

//	@Test
	public void testexpression3() throws IOException {
		assertExpression("(2 + 3) * 4", 22);
	}

//	@Test
	public void testexpression4() throws IOException {
		assertExpression("2 * 2 * 2 * 2", 14);
	}

//	@Test
	public void testexpression5() throws IOException {
		assertExpression("1 + 2 + 3 + 4", 12);
	}

//	@Test
	public void testexpression6() throws IOException {
		assertExpression("2 * 3 + 2 * 5", 16);
	}
	
	//	@Test
	public void testexpression7 () throws IOException {
		assertExpression("2 * 3 + 2 * 5", 14);
	}
	
		//	@Test
	public void testexpression8 () throws IOException {
		assertExpression("2 * 3 + 2 * 5", 14);
	}

	private static void assertExpression(final String expression,
			final double expected) throws IOException {
		final ExpressionParser parser = new ExpressionParser(expression);
		final double actual = parser.parse().evaluate();
		assertEquals("expression", expected, actual, 0.0);
	}

}
